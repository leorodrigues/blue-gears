
#include <napi.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#define MAX_RESP_SIZE 255

Napi::Object SetBoolProperty(
    Napi::Env environment,
    Napi::Object object,
    const char *propertyName,
    bool propertyValue) {

    object.Set(Napi::String::New(environment, propertyName), propertyValue);

    return object;
}

Napi::Object SetStrProperty(
    Napi::Env environment,
    Napi::Object object,
    const char *propertyName,
    const char *propertyValue) {

    object.Set(
        Napi::String::New(environment, propertyName),
        Napi::String::New(environment, propertyValue));

    return object;
}

Napi::Object SetFuncProperty(
    Napi::Env environment,
    Napi::Object object,
    const char *propertyName,
    Napi::Object propertyValue(const Napi::CallbackInfo &)) {

    object.Set(
        Napi::String::New(environment, propertyName),
        Napi::Function::New(environment, propertyValue));

    return object;
}

Napi::Object SetObjProperty(
    Napi::Env environment,
    Napi::Object object,
    const char *propertyName,
    Napi::Object propertyValue) {

    object.Set(
        Napi::String::New(environment, propertyName),
        propertyValue);

    return object;
}

Napi::Object SetArrayProperty(
    Napi::Env environment,
    Napi::Object object,
    const char *propertyName,
    Napi::Array propertyValue) {
    
    object.Set(
        Napi::String::New(environment, propertyName),
        propertyValue);
    
    return object;
}

Napi::Object MakeError(Napi::Env env, const char *messageString) {
    return SetStrProperty(env, Napi::Object::New(env), "token", messageString);
}

Napi::Object Scan(const Napi::CallbackInfo& info) {
    Napi::Array devices;
    Napi::Env env = info.Env();
    Napi::Object scanResult = Napi::Object::New(env);

    int dev_id = hci_get_route(NULL);

    if (dev_id < 0)
        return SetObjProperty(env, scanResult, "error",
            MakeError(env, "hci_get_route.failed"));

    int sock = hci_open_dev(dev_id);

    if (sock < 0)
        return SetObjProperty(env, scanResult, "error",
            MakeError(env, "hci_open_dev.failed"));

    int len = 9;
    int flags = IREQ_CACHE_FLUSH;

    inquiry_info *inquiry = (inquiry_info *)malloc(
        MAX_RESP_SIZE * sizeof(inquiry_info));

    int respSize = hci_inquiry(
        dev_id, len, MAX_RESP_SIZE, NULL, &inquiry, flags);

    if (respSize < 0)
        return SetObjProperty(env, scanResult, "error",
            MakeError(env, "hci_inquiry.failed"));
    else if (respSize > 0) {
        devices = Napi::Array::New(env, respSize);
        SetObjProperty(env, scanResult, "devices", devices);
    }

    char addr[19] = { 0 };
    char name[248] = { 0 };

    for (int i = 0; i < respSize; i++) {
        Napi::Object device = Napi::Object::New(env);
        devices[i] = device;

        ba2str(&(inquiry + i)->bdaddr, addr);
        memset(name, 0, sizeof(name));
        if (hci_read_remote_name(sock, &(inquiry + i)->bdaddr, sizeof(name), name, 0) < 0)
            strcpy(name, "[unknown]");
        
        SetStrProperty(env, device, "address", addr);
        SetStrProperty(env, device, "name", name);
    }

    free(inquiry);

    return scanResult;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    return SetFuncProperty(env, exports, "scan", Scan);
}

NODE_API_MODULE(bluezdadapter, Init);
