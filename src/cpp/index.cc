
#include <napi.h>

#include "hci.hh"

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
    return HCI::Init(env, exports);
}

NODE_API_MODULE(bluez_adapter, InitAll)