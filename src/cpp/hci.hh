
#ifndef _HCI_
#define _HCI_

#include <napi.h>

class HCI : public Napi::ObjectWrap<HCI> {

public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);

    HCI(const Napi::CallbackInfo& callbackInfo);

private:
    static Napi::FunctionReference constructor;

};

#endif