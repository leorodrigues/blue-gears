
#include "hci.hh"

Napi::FunctionReference HCI::constructor;

Napi::Object HCI::Init(Napi::Env env, Napi::Object exports) {
    Napi::HandleScope scope(env);
    Napi::Function fConstructor = DefineClass(env, "HCI", { });

    constructor = Napi::Persistent(fConstructor);
    constructor.SuppressDestruct();

    exports.Set("HCI", fConstructor);

    return exports;
}

HCI::HCI(const Napi::CallbackInfo& callbackInfo)
    : Napi::ObjectWrap<HCI>(callbackInfo) {

}
