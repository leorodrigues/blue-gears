
class FetchBinding {
    constructor({ bindingCollection, domainKit }) {
        this.bindingCollection = bindingCollection;
        this.domainKit = domainKit;
    }

    async execute() {
        const binding = this.domainKit.makeBinding();
        binding.fetchFrom(this.bindingCollection);
    }
}

module.exports = { FetchBinding };