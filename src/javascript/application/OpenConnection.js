
class OpenConnection {
    constructor({ bindingCollection, domainKit }) {
        this.bindingCollection = bindingCollection;
        this.domainKit = domainKit;
    }

    async execute(address) {
        const connection = this.domainKit.makeConnection();
        connection.open(address);
    }
}

module.exports = { OpenConnection };