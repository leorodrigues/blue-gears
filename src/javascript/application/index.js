module.exports = {
    ...require('./BindConsole'),
    ...require('./FetchBinding'),
    ...require('./OpenConnection'),
    ...require('./ScanForConsoleCandidate')
};