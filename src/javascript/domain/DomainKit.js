
const { Connection } = require('./Connection');
const { Console } = require('./Console');
const { Binding } = require('./Binding');

class DomainKit {

    constructor(events) {
        this.events = events;
    }

    /* istanbul ignore next */
    makeConnection() {
        return new Connection(this.events);
    }

    /* istanbul ignore next */
    makeConsole() {
        return new Console(this.events);
    }

    /* istanbul ignore next */
    makeBinding() {
        return new Binding(this.events);
    }
}

module.exports = { DomainKit };