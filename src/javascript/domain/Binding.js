
class Binding {

    constructor(events) {
        this.events = events;
    }

    fetchFrom(bindingCollection) {
        const binding = bindingCollection.findLast();
        if (binding)
            this.events.fireBindingFetched(binding);
        else
            this.events.fireFailedToFetchBinding();
    }
}

module.exports = { Binding };