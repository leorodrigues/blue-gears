{
    "targets": [
        {
            "target_name": "bluez-adapter",
            "cflags!": [ "-fno-exceptions" ],
            "cflags_cc!": [ "-fno_exceptions" ],
            "include_dirs": [
                "<!@(node -p \"require('node-addon-api').include\")",
                "./node_modules/node-addon-api/"
            ],
            "dependencies": [
                "<!@(node -p \"require('node-addon-api').gyp\")"
            ],
            "sources": [
                "src/cpp/index.cc",
                "src/cpp/hci.cc"
            ],
            "defines": [
                "NAPI_DISABLE_CPP_EXCEPTIONS"
            ],
            "libraries": [
                "-lbluetooth"
            ]
        }
    ]
}