const resify = require('restify');

const server = resify.createServer();

server.get('/hello', (req, res) => res.json({ msg: 'hello world' }));

server.listen(3000, () => console.log('Running...'));